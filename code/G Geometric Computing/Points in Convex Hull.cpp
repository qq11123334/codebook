bool isInsideConvexPolygon(vector<Point>& hull, Point& p){
    int n = hull.size();
    if(n == 2) {
        ll ux = max(hull[0].x, hull[1].x), lx = min(hull[0].x, hull[1].x);
        ll uy = max(hull[0].y, hull[1].y), ly = min(hull[0].y, hull[1].y);
        return (cross(p, hull[0], hull[1]) == 0 && lx <= p.x && p.x <= ux && ly <= p.y && p.y <= uy);
    }
    if(cross(hull[0], hull[1], p) < 0 || cross(hull[0], hull[n-1], p) > 0) return false;
    int l = 1, r = n - 1;
    while(l + 1 < r){
        int m = (l + r) / 2;
        if(cross(hull[0], hull[m], p) >= 0) l = m;
        else r = m;
    }
    return cross(hull[l], hull[r], p) >= 0;
}