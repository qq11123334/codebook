const int LEFT = 1, RIGHT = 2, ON_LINE = 3;
int PointTest(Point a, Point b, Point p) {
    ll res = cross(a, b, p);
    if(res > 0) return LEFT;
    if(res < 0) return RIGHT;
    if(res == 0) return ON_LINE;
}
bool Intersect(Point a, Point b, Point p) {
    bool res1 = (a.x <= p.x && p.x <= b.x) || (b.x <= p.x && p.x <= a.x);
    bool res2 = (a.y <= p.y && p.y <= b.y) || (b.y <= p.y && p.y <= a.y);
    return res1 && res2;
}
bool IsSegIntersect(Point a, Point b, Point p, Point q) {
    int Tp = PointTest(a, b, p), Tq = PointTest(a, b, q);
    int Ta = PointTest(p, q, a), Tb = PointTest(p, q, b);
    bool Ip = Intersect(a, b, p), Iq = Intersect(a, b, q);
    bool Ia = Intersect(p, q, a), Ib = Intersect(p, q, b);
    /* case for on line */
    if(Tp == ON_LINE && Ip) return true;
    if(Tq == ON_LINE && Iq) return true;
    if(Ta == ON_LINE && Ia) return true;
    if(Tb == ON_LINE && Ib) return true;
    /* case for not on line */
    return (Tp != Tq) && (Ta != Tb);
}