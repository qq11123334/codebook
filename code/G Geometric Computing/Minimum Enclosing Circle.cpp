Point excenter(Point a, Point b, Point c, double &r) {
    double a2 = a.x * a.x + a.y * a.y;
    double b2 = b.x * b.x + b.y * b.y;
    double c2 = c.x * c.x + c.y * c.y;
    double d = 2 * (a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y));
    double cx = (a2 * (b.y - c.y) + b2 * (c.y - a.y) + c2 * (a.y - b.y)) / d;
    double cy = (a2 * (c.x - b.x) + b2 * (a.x - c.x) + c2 * (b.x - a.x)) / d;
    Point center(cx, cy);
    r = abs(center - a);
    return center;
}
Point Minimum_Enclosing_Circle(vector<Point> pts, double &r) {
    Point cent;
    random_shuffle(pts.begin(), pts.end());
    cent = pts[0], r = 0;
    for (int i = 1; i < pts.size(); ++i)
        if (abs(pts[i] - cent) > r) {
            cent = pts[i], r = 0;
            for (int j = 0; j < i; ++j)
                if (abs(pts[j] - cent) > r) {
                    cent = (pts[i] + pts[j]) / 2;
                    r = abs(pts[i] - cent);
                    for (int k = 0; k < j; ++k)
                        if (abs(pts[k] - cent) > r)
                            cent = excenter(pts[i], pts[j], pts[k], r);
                }
        }
    return cent;
}