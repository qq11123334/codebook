ll Polygon_Area(vector<Point> &point) {
    ll ans = 0; int n = (int)point.size();
    for(int i = 0;i < n;i++) {
        Point p1 = {point[i].x, point[i].y};
        Point p2 = {point[(i + 1) % n].x, point[(i + 1) % n].y};
        ans += cross(p1, p2);
    }
    return ans > 0 ? ans : -ans;
}