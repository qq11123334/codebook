vector<Point> Half_Hull(vector<Point> &point) {
    vector<Point> half_hull;
    for(int i = 0; i < (int)point.size(); i++) {
        int sz;
        while((sz = (int)half_hull.size()) > 1 && cross(half_hull[sz - 2], point[i], half_hull[sz - 1]) < 0)
            half_hull.pop_back();
        half_hull.push_back(point[i]);
    }
    return half_hull;
}
vector<Point> Convex_Hull(vector<Point> point) {
    sort(point.begin(), point.end(), [](Point a, Point b) {
        if(a.x != b.x) return a.x < b.x;
        return a.y < b.y;
    });
    vector<Point> upper = Half_Hull(point);
    reverse(point.begin(), point.end());
    vector<Point> lower = Half_Hull(point);
    // the convex hull order : upper[0 ~ upper.size() - 2] + lower[0 ~ lower.size() - 2]; (the upper[0] is equal with lower.back)
    if(upper.size() >= 1LL) upper.pop_back();
    if(lower.size() >= 1LL) lower.pop_back();
    vector<Point> Convex(upper.begin(), upper.end());
    Convex.insert(Convex.end(), lower.begin(), lower.end());
    return Convex;
}