double PointSegDist(Point &p1, Point &p2, Point &p) {
    ll dx = p1.x - p2.x, dy = p1.y - p2.y;
    Point pr;
    pr.x = p.x + dy;
    pr.y = p.y - dx;
    bool res1 = (cross(p, pr, p1) >= 0);
    bool res2 = (cross(p, pr, p2) >= 0);
    if(res1 == res2) return min(len(p, p1), len(p, p2));
    else return fabs(cross(p, p1, p2)) / len(p1, p2);
}