// ll or double
struct Point { 
    ll x, y;
    int i;
    Point(ll _x, ll _y, int _i = 0) : x(_x), y(_y), i(_i) {}
    friend Point operator +(const Point& p, const Point& q) {
        return Point(p.x + q.x, p.y + q.y); }
    friend Point operator - (const Point& p, const Point& q) { 
        return Point(p.x - q.x, p.y - q.y); }
    friend Point operator * (const Point& p, const ll& k) { 
        return Point(p.x * k, p.y * k); } 
    friend ll dot(const Point& p, const Point& q) {
        return p.x * q.x + p.y * q.y; }
    friend ll cross(const Point& p, const Point& q) { 
        return p.x * q.y - p.y * q.x; }
    friend double abs(Point a) { return sqrt(dot(a, a)); }
};
ll cross(Point a, Point b, Point c) { return cross(b - a, c - a); }
// two lines f(x) = m * x + c
// the intersection point
// x = (c2 - c1) / (m1 - m2)
// y = (m2c1 - m1c2) / (m2 - m1)