// include Simple Big Integer multiplication
ll g(ll x, ll rnd, ll p) {
    return (rnd * muti(x, x, p) + 1) % p;
}
map<ll, int> fac;
void pollard_rho(ll n) {
    if(!is_prime(n) && n > 1) {
        ll d;
        while(1) {
            ll x = unif(gen) % n;
            ll rnd = unif(gen) % 1000;
            ll y = g(x, rnd, n);
            d = 1;
            while(d == 1 && x != y) {
                x = g(x, rnd, n);
                y = g(g(y, rnd, n), rnd, n);
                d = gcd(abs(x - y), n);
            }
            if(d == n) continue;
            break;
        }
        pollard_rho(d);
        pollard_rho(n / d);
    } else if(n > 1)
        fac[n]++;
}