const ll N = 1e6; // must be Composite
bool is_prime[N];
vector<ll> primes;
void init_primes() {
    fill(is_prime, is_prime + MAX_NUM, true);
    is_prime[0] = is_prime[1] = false;
    for (ll i = 2; i < N; i++) {
        if (is_prime[i]) {
            primes.push_back(i);
            for (ll j = i * i; j < N; j += i)
                is_prime[j] = false;
        }
    }
}