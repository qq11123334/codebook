int mobius[N];
int LPF[N], cnt[N];
void Least_Prime_Factor() {
    for(int i = 2;i <= N;i++) {
        if(!LPF[i]) {
            for(int j = i;j <= N;j += i) {
                if(!LPF[j]) LPF[j] = i;
            }
        }
    }
}
void Mobius() {
    Least_Prime_Factor();
    mobius[1] = 1;
    for(int i = 2;i <= N;i++) {
        if(LPF[i / LPF[i]] == LPF[i]) {
            mobius[i] = 0;
        } else {
            mobius[i] = -1 * mobius[i / LPF[i]];
        }
    }
}