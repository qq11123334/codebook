ll fp(ll x, ll y, ll p) {
    ll res = 1;
    while(y) {
        if(y & 1) res = res * x % p;
        y >>= 1;
        x = x * x % p;
    }
    return res;
}
ll muti(ll x, ll y, ll p) { // x * y % p
    ll res = 0;
    while(y) {
        if(y & 1) res = (res + x) % p;
        y >>= 1;
        x = x * 2 % p;
    }
    return res;
}