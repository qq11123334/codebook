// Use Gaussian Elimination
ll Determinant(vector<vector<ll>> &matrix) {
	int n = matrix.size();
	int m = matrix[0].size();
	int row = 0;
	ll ans = 1;
	for(int col = 0; col < m; col++) {
		ll nonezero_row = -1;
		for(int i = row; i < n; i++) {
			if(matrix[i][col]) {
				nonezero_row = i;
				break;
			}
		}
		if(nonezero_row == -1) continue;
		if(row != nonezero_row) ans *= -1;
		swap(matrix[row], matrix[nonezero_row]);
		for(int i = row + 1; i < n; i++) {
			if(matrix[i][col] == 0) continue;
			ll lm = lcm(matrix[row][col], matrix[i][col]);
			ll frac1 = lm * inv(matrix[row][col]) % MOD;
			ll frac2 = lm * inv(matrix[i][col]) % MOD;
			for(int j = 0; j < m; j++) {
				matrix[i][j] *= frac2;
				matrix[i][j] -= (matrix[row][j] * frac1 % MOD);
				matrix[i][j] = (matrix[i][j] % MOD + MOD) % MOD;
			}
			ans *= inv(frac2);
			ans %= MOD;
		}
		row++;
	}
	for(int i = 0; i < n; i++) {
		ans *= matrix[i][i];
		ans %= MOD;
	}
	return (ans % MOD + MOD) % MOD;
    for(int row = n - 1; row >= 0; row--) {
        for(int col = 0; col < m - 1; col++) {
            if(matrix[row][col] == 1) {
                for(int i = row - 1; i >= 0; i--) {
                    ll mul = matrix[i][col];
                    for(int j = 0; j < m; j++) {
                        matrix[i][j] -= matrix[row][j] * mul;
						matrix[i][j] = (matrix[i][j] % MOD + MOD) % MOD;
                    }
                }
                break;
            }
        }
    }
}