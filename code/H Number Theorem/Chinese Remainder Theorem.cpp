ll inv(ll n, ll p) {
	ll x, y, z;
	tie(x, y, z) = exgcd(n, p);
	return (x % p + p) % p;
}
// ans % eqs[i].second = eqs[i].first 
ll CRT(vector<pair<ll, ll>> eqs) {
	ll M = 1, ans = 0, n = eqs.size();
	for(int i = 0; i < n; i++)
		M = M * eqs[i].second;
    for(int i = 0; i < n; i++) {
        ll Mi = M / eqs[i].second;
        ans += eqs[i].first * inv(Mi, eqs[i].second) % M * Mi;
		ans %= M;
    }
    return ans;
}