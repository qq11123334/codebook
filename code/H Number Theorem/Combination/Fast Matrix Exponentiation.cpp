// a_i=a_i-1+a_i-2+a_i-3+a_i-4+a_i-5+a_i-6
using Matrix = array<array<ll, N>, N>;
Matrix matrix_muti(Matrix x, Matrix y, ll p) {
    Matrix c = zero();
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            for(int k = 0; k < n; k++)
                c[i][j] = (c[i][j] + x[i][k] * y[k][j]) % p;
        }
    }
    return c;
}
Matrix fp(Matrix x, ll y, ll p) {
    Matrix res = I();
    while(y) {
        if(y & 1) res = matrix_muti(x, res, MOD);
        y >>= 1;
        x = matrix_muti(x, x, MOD);
    }
    return res;
}