#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops,fast-math,O3")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
cin.tie(0)->sync_with_stdio(false);
stringstream ss; string s;
getline(cin, s);
ss << s;
#include <cctype> <cmath> <cstdio> <cstdlib> <cstring> <map> <queue> <stack> <set> <string> <utility> <vector>
__int128
/*
g++ -Wshadow -Wall -O2 file.cpp -g
g++ -x c++ -Wall -std=c++17 -O2 -static -pipe -o "$DEST" "$@"
gdb a.out
b main : breakpoint at main
n : next one line (skip function)
s : next one line (into function)
c : continue
* bt : list current stack
up : go to the up stack
down : go to the down stack
*/
default_random_engine gen(time(NULL));
uniform_int_distribution<ll> unif(0, (ll)1e18);
uniform_real_distribution<double> unif(0, 1);
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag, tree_order_statistics_node_update> indexed_set;
// less(set) or less_equal(multiset)
indexed_set s;
s.insert(2), s.insert(3), s.insert(7), s.insert(9);
cout << *s.find_by_order(2) <<; // 7
cout << s.order_of_key(7) << "\n"; // 2
cout << s.order_of_key(6) << "\n"; // 2
cout << s.order_of_key(8) << "\n"; // 3
cout << __builtin_popcount(10) << "\n";
