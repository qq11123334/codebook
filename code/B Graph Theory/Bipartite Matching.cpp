int match[N]; bool used[N]; vector<int> adj[N];
bool DFS(int x) {
    for(auto u : adj[x]) {
        if(used[u]) continue;
        used[u] = 1;
        int next = match[u];
        if(next == -1 || DFS(next)) {
            match[u] = x;
            return 1;
        }
    }
    return 0;
}
int n, m; /* left size and right size */
int Bipartite_match() {
    memset(match, -1, sizeof(match));
    memset(used, 0, sizeof(used));
    int match_number = 0;
    for(int i = 1; i <= n; i++) {
        if(DFS(i)) {
            memset(used, 0, sizeof(used));
            match_number++;
        }
    }
    // and if(match[i] != -1) {i(right set) and match[i](left set) match}
    return match_number;
}
void add_edge(int left_node, int right_node) 
{ adj[left_node].push_back(right_node); }