// Edge = {a->b, w}
vector<Edge> edge;
ll dis[N], n, m;
ll Bellman_Ford(int s, int t) {
    // if return INF, doesn't exist shortest path (s -> t)
	memset(dis, 0x3f, sizeof(dis));
	dis[s] = 0;
	int times = 2 * n;
	for (int i = 1; i <= times; i++) {
		for (auto e : edge) {
			int a = e.a, b = e.b; ll w = e.w;
			if (dis[a] == INF) continue;
			if (dis[b] > dis[a] + w) {
				dis[b] = dis[a] + w;
				if (i >= n) {
					if(dis[a] < 0) dis[a] = -INF;
					if(dis[b] < 0) dis[b] = -INF;
					if (b == t) return INF;
				}
			}
		}
	}
	return dis[t];
}