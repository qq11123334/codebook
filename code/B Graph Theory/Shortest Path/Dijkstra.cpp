priority_queue <pii, vector<pii>, greater<pii>> pq;
memset(dis, 0x3f, sizeof(dis));
dis[s] = 0; pq.push(make_pair(0, s));
while(!pq.empty()) {
    auto P = pq.top(); pq.pop();
    int x = P.second; ll _dis = P.first;
    if(dis[x] < _dis) continue;
    for(auto &[v, w] : adj[x]) {
        if(dis[v] > dis[x] + w) {
            dis[v] = dis[x] + w;
            pq.push(make_pair(dis[v], v));
        }
    }
}