void Floyd_Warshell() {
    // init: dis[i][j] = INF, dis[i][i] = 0
    // add_edge: dis[a][b] = min(dis[a][b], w) 
    for(int k = 1; k <= n; k++) {
        for(int i = 1; i <= n; i++) {
            for(int j = 1; j <= n; j++) {
                dis[i][j] = min(dis[i][j], dis[i][k] + dis[k][j]);
            }
        }
    }
}