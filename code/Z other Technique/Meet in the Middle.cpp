map<ll, int> subset_sum_cnt;
void DFS1(int level, ll sum) {
    if(sum > x) return;
    if(level == n / 2) {
        subset_sum_cnt[sum]++;
        return;
    }
    DFS1(level + 1, sum + arr[level]);
    DFS1(level + 1, sum);
}
vector<int> sum_list;
void DFS2(int level, ll sum) {
    if(sum > x) return;
    if(level == n) {
        sum_list.push_back(sum);
        return;
    }
    DFS2(level + 1, sum + arr[level]);
    DFS2(level + 1, sum);
}
ll solve() {
    DFS1(0, 0); DFS2(n / 2, 0);
    ll ans = 0;
    for(auto s : sum_list)
        ans += subset_sum_cnt[x - s];
    return ans;
}