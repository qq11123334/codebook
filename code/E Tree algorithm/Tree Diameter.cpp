void DFS(int x, ll dis, ll &mx, ll p) {
    dep[x] = dis;
    if(dep[mx] <= dep[x]) mx = x;
    for(auto v : adj[x]) {
        if(v == p) continue;
        DFS(v, dis + 1, mx, x);
    }
}
ll Tree_Diameter() {
    ll c = 1;
    DFS(1, 0, c, -1);
    DFS(c, 0, c, -1);
    return dep[c];
}