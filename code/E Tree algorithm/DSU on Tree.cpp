void DFS(int x, int par) {
    subtree_color[x][color[x]]++;
    for(auto v : adj[x]) {
        if(v != par) {
            DFS(v, x);
            if(subtree_color[v].size() > subtree_color[x].size())
                swap(subtree_color[v], subtree_color[x]);
            for(auto p : subtree_color[v]) { // merge
                subtree_color[x][p.first] += p.second;
            }
        }
    }
    ans[x] = (int)subtree_color[x].size(); // offline
}