void DFS(int x) { // use data structure to maintain tree_arr
    vis[x] = 1; sz[x] = 1;
    node_to_index[x] = (int)tree_arr.size(); 
    tree_arr.push_back(x);
    for(auto v : adj[x]) {
        if(!vis[v]) {
            DFS(v);
            sz[x] += sz[v];
        }
    }
}