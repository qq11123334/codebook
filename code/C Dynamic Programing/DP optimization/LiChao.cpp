struct Line {
    ll a, b; // ax + b
    ll val(ll x) { return a * x + b; } 
    Line() {}
    Line(ll _a, ll _b) : a(_a), b(_b) {}
};
struct Node {
    int L, R;
    Line line;
    Node *lc, *rc;
};
void build(Node *r, int L, int R) {
    r->L = L, r->R = R;
    r->line = Line(INF, INF);
    if(L == R) return;
    int M = (L + R) / 2;
    build(r->lc = new Node(), L, M);
    build(r->rc = new Node(), M + 1, R);
}
void update(Node *r, Line line) {
    Line left_line, right_line;
    if(line.a > r->line.a) {
        left_line = line;
        right_line = r->line;
    } else {
        left_line = r->line;
        right_line = line;
    }
    int M = (r->L + r->R) / 2;
    if(left_line.val(M) < right_line.val(M)) {
	    r->line = left_line;
	    if(r->L != r->R) update(r->rc, right_line);
    } else {
        r->line = right_line;
        if(r->L != r->R) update(r->lc, left_line);
    }
}
ll qry(Node *r, int x) {
    if(r->L == r->R) return r->line.val(x);
    int M = (r->L + r->R) / 2;
    if(x <= M) return min(r->line.val(x), qry(r->lc, x));
    else return min(r->line.val(x), qry(r->rc, x));
}