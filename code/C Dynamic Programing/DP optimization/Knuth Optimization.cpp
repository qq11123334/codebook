/* Given an array of n numbers, your task is to divide it into n number */
/* On one move, choose any subarray and split it into two subarrays.*/
/* The cost of such a move is the sum of values in the chosen subarray. */
/* pos[left][right] = best split position (left < pos[left][right] < right) */
/* dp[left][right] = minimum cost the split every number in (left, right) */
for(int i = 1;i <= n;i++) { pos[i][i] = i; }
for(int l = 2;l <= n;l++) {
    for(int i = 1;i + l - 1 <= n;i++) {
        // compute dp[i][i + l - 1]
        int left = i, right = i + l - 1;
        pair<ll, int> best;
        best = make_pair(INF, -1);
        for(int p = pos[left][right - 1];p <= pos[left + 1][right];p++)
            best = min(best, make_pair(dp[left][p] + dp[p + 1][right] + cost(left, right), p));
        pos[left][right] = best.second;
        dp[left][right] = best.first;
    }
}