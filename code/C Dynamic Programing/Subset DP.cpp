/* Give weight of each person and the maximum allowed weight in the elevator. 
 * Find the minimum number of elevator rides
 * dp[subset] = {minimum rides, the last rides weight}*/
dp[0] = {1, 0};
for(int s = 1;s < (1 << n);s++) {
    dp[s] = {INF, INF};
    for(int i = 0;i < n;i++) {
        if(s & (1 << i)) {
            pair<ll, ll> option = dp[s ^ (1 << i)];
            if(option.second + weight[i] > x) {
                option.first++;
                option.second = weight[i];
            } else {
                option.second += weight[i];
            }
            dp[s] = min(dp[s], option);
        }
    }
}