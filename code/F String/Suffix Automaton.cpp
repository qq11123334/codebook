int state[N][W], link_to[N], length[N];
int last_state = 0, new_state = 1;
int clone(int x) {
    int z = new_state++;
    for(int i = 0; i < W; i++)
        state[z][i] = state[x][i];
    link_to[z] = link_to[x];
    return z;
}
void SA(string &s) {
    int n = (int)s.size();
    link_to[0] = -1;
    for(int i = 0; i < n; i++) {
        int c = (s[i] - 'a');
        int x = last_state;
        int y = new_state++;
        last_state = y;
        length[y] = length[x] + 1;
        link_to[y] = 0;
        int s;
        for(s = x; s != -1 && state[s][c] == 0; s = link_to[s])
            state[s][c] = y;
        if(s == -1) continue;
        int u = state[s][c];
        if(length[s] + 1 == length[u]) {
            link_to[y] = u;
            continue;
        }
        int z = clone(u);
        length[z] = length[s] + 1;
        for(int j = s; j != -1 && state[j][c] == u; j = link_to[j])
            state[j][c] = z;
        link_to[u] = link_to[y] = z;
    }
    for(int i = last_state; i != -1; i = link_to[i])
        dp1[i] = 1;
}