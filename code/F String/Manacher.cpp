int P[2 * N];
void Manacher(string &ss) {
    string s;
    s.resize(2 * (int)ss.size() + 1, '.');
    for(int i = 0;i < (int)ss.size();i++)
        s[2 * i + 1] = ss[i];
    // ss = abacabd, s = .a.b.a.c.a.b.d.
    int l = 0, r = 0, n = (int)s.size();
    for(int i = 0;i < n;i++) {
        P[i] = max(min(P[l + l - i], r - i), 1);
        while(0 <= i - P[i] && i + P[i] < n && s[i - P[i]] == s[i + P[i]]) {
            P[i]++;
            l = i, r = i + P[i];
        }
    }
    /* P[i] is the Radius of longest Palindrome that center is Position i
     * s = . a . b . a . c . a . b . d .
     * P = 1 2 1 4 1 2 1 6 1 2 1 2 1 2 1 */
}