int n; ll arr[N], pre[N], BIT[N];
// BIT[i] = pre[i] - pre[i - lowbit(i)]
void upd(ll x, ll dif) {
    for(int i = x; i <= n; i += (i & -i))
        BIT[i] += dif;
}
ll sum(ll x) { // prefix_sum 1~x
    ll res = 0;
    for(int i = x; i > 0; i -= (i & -i)) {
        res = res + BIT[i];
    }
    return res;
}