/* This is for Sum Query and Reverse Query
 * Sum Query : Split root to Left, Mid, Right, ans = Mid->sum
 * Reverse Query : Split root to Left, Mid, Right, and mark swap_tag at Mid and Merge back into a root
 * use srand(time(0) * clock()); */
struct Treap {
    int size, pri, swap_tag;
    ll val, sum;
    Treap *lc, *rc;
    Treap(ll num) {
        swap_tag = 0, size = 1; 
        pri = rand();
        val = sum = num;
        lc = rc = NULL;
    }
};
int size(Treap *node) {
    if(node == NULL) return 0;
    else return node->size;
}
ll sum(Treap *node) {
    if(node == NULL) return 0;
    else return node->sum;
}
void push(Treap *node) {
    assert(node != NULL);
    if(node != NULL) {
        if(node->swap_tag) {
            if(node->lc != NULL) node->lc->swap_tag ^= 1;
            if(node->rc != NULL) node->rc->swap_tag ^= 1;
            swap(node->lc, node->rc);
        }
        node->swap_tag = 0;
    }
}
void split(Treap *node, Treap *&left, Treap *&right, int k) {
    if(node == NULL) left = right = NULL;
    else {
        push(node);
        if(size(node->lc) < k) {
            split(node->rc, node->rc, right, k - size(node->lc) - 1);
            left = node;
        } else {
            split(node->lc, left, node->lc, k);
            right = node;
        }
        node->size = size(node->lc) + size(node->rc) + 1;
        node->sum = sum(node->lc) + sum(node->rc) + node->val;
    }
}
void merge(Treap *&node, Treap *left, Treap *right) {
    if(left == NULL) node = right;
    else if(right == NULL) node = left;
    else {
        if(left->pri < right->pri) {
            push(left);
            merge(left->rc, left->rc, right);
            node = left;
        } else {
            push(right);
            merge(right->lc, left, right->lc);
            node = right;
        }
        node->size = size(node->lc) + size(node->rc) + 1;
        node->sum = sum(node->lc) + sum(node->rc) + node->val;
    }
}