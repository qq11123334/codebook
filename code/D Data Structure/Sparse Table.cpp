void build_sparse_table() {
    for(int i = 0;i < n;i++) sp[0][i] = arr[i];
    for(int i = 1;(1 << i) <= n;i++) {
        for(int j = 0;j + (1 << i) <= n;j++) {
            sp[i][j] = min(sp[i - 1][j], sp[i - 1][j + (1 << (i - 1))]);
        }
    }
}
ll query(int left, int right) {
    // or int_log2
    int k = log2(right - left + 1);
    return min(sp[k][left], sp[k][right - (1 << k) + 1]);
}