ll grid[N][N], BIT[N][N]; int n;
ll sum(int x, int y) { // sum of rectangle (1, 1) ~ (x, y)
    ll res = 0;
    for(int i = x; i > 0; i -= (i & -i)) {
        for(int j = y; j > 0; j -= (j & -j))
            res += BIT[i][j];
    }
    return res;
}
void upd(int x, int y, ll dif) { // update the value of (x, y) by dif  
    for(int i = x; i <= n; i += (i & -i)) {
        for(int j = y; j <= n; j += (j & -j))
            BIT[i][j] += dif;
    }
}