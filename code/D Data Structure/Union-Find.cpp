int p[N], sz[N];
int find(int x) {
    if(p[x] == x) return x;
    return p[x] = find(p[x]);
}
void unite(int a,int b) {
    a = find(a); b = find(b);
    if(a == b) return;
    if(sz[a] > sz[b]) swap(a, b);
    p[a] = p[b];
    sz[b] += sz[a];
}
void init() {
    for(int i = 1; i <= N; i++)
        sz[i] = 1; p[i] = i;
}
