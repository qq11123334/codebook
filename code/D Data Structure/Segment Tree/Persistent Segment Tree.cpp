ll arr[N];
struct Node {
    int L, R;
    ll sum;
    Node *lc, *rc;
    Node() {L = R = sum = 0, lc = rc = NULL;}
    void pull() { sum = lc->sum + rc->sum; }
}*root_node[N]; // Store the history root
/* Use new_root = copy_node(old_root)
 * and Update new_root */
Node *copy_node(Node *r) {
    Node* cp = new Node();
    cp->L = r->L; cp->R = r->R;
    cp->sum = r->sum;
    cp->lc = r->lc; cp->rc = r->rc;
    return cp;
}
void build(Node *r, int L, int R) {
    r->L = L; r->R = R;
    if(L == R) {
        r->sum = arr[L]; 
        return;
    }
    int M = (L + R) / 2;
    build(r->lc = new Node(), L, M);
    build(r->rc = new Node(), M + 1, R);
    r->pull();
}
void upd(Node *r, Node *new_r, int pos, ll val) {
    if(r->L == r->R) {
        new_r->sum = val;
        return;
    }
    int M = (r->L + r->R) / 2;
    if(pos <= M) {
        new_r->lc = copy_node(r->lc);
        upd(r->lc, new_r->lc, pos, val);
    }
    else {
        new_r->rc = copy_node(r->rc);
        upd(r->rc, new_r->rc, pos, val);
    }
    new_r->pull();
}
ll qry(Node *r, int ql, int qr) {
    if(ql > r->R || qr < r->L) return 0;
    if(ql <= r->L && r->R <= qr) return r->sum;
    return qry(r->lc, ql, qr) + qry(r->rc, ql, qr);
}