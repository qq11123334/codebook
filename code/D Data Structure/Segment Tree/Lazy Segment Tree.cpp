// RMQ and range update
struct Node {
    int L, R;
    ll val, lazy_val;
    Node *lc, *rc;
    void pull() {
        val = min(lc->val, rc->val);
    }
};
void push(Node *r) {
    if(!r->lazy_val) return;
    if(r->L != r->R) {
        r->lc->val += r->lazy_val;
        r->rc->val += r->lazy_val;
        r->lc->lazy_val += r->lazy_val;
        r->rc->lazy_val += r->lazy_val;
    }
    r->lazy_val = 0;
}
ll arr[N];
void build(Node *r, int L, int R) {
    r->L = L; r->R = R; 
    int M = (L + R) / 2;
    r->lazy_val = 0;
    if(L == R) {
        r->val = arr[L];
        return;
    }
    build(r->lc = new Node(), L, M);
    build(r->rc = new Node(), M + 1, R);
    r->pull();
}
void upd(Node *r, int ql, int qr, ll val) {
    if(ql > r->R || qr < r->L) return;
    if(ql <= r->L && r->R <= qr) {
        r->val += val;
        r->lazy_val += val;
        return;
    }
    push(r);
    upd(r->lc, ql, qr, val);
    upd(r->rc, ql, qr, val);
    r->pull(); // important
}
ll qry(Node *r, int ql, int qr) {
    if(ql > r->R || qr < r->L) return INF;
    if(ql <= r->L && r->R <= qr) return r->val;
    push(r);
    return min(qry(r->lc, ql, qr), qry(r->rc, ql, qr));
}