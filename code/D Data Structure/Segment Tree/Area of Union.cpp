struct Event {
    ll x; ll y1, y2;
    string type;
    Event(ll _x, ll _y1, ll _y2, string _type): x(_x), y1(_y1), y2(_y2), type(_type) {} 
    bool operator < (const Event a) {
        return x < a.x;
    }
};
map<ll, ll> compress;
vector<ll> id_to_cor;
vector<Event> SweepLine;
struct Node {
    int L, R; ll uni, num;
    Node *lc, *rc;
    void pull() {
        if(num) uni = id_to_cor[R] - id_to_cor[L];
        else if(L != R - 1) uni = lc->uni + rc->uni;
        else uni = 0;
    }
};
void build(Node *r, int L, int R) {
    r->L = L, r->R = R; r->uni = 0;
    if(L == R - 1) return;
    int M = (L + R) / 2;
    build(r->lc = new Node(), L, M);
    build(r->rc = new Node(), M, R);
}
ll qry_uni(Node *r) { return r->uni; }
void upd(Node *r, int ql, int qr, ll dif) {
    if(ql >= r->R || qr <= r->L) return;
    if(ql <= r->L && r->R <= qr) {
        r->num += dif; 
        r->pull();
        return;
    }
    upd(r->lc, ql, qr, dif);
    upd(r->rc, ql, qr, dif);
    r->pull();
}
struct Rectangle {
    ll x1, x2, y1, y2;
};
ll Area_of_Union(vector<Rectangle> v) {
    for(auto Rec : v) {
        ll x1 = Rec.x1, x2 = Rec.x2; 
        ll y1 = Rec.y1, y2 = Rec.y2;
        compress[y1], compress[y2];
        SweepLine.push_back(Event(x1, y1, y2, "Add"));
        SweepLine.push_back(Event(x2, y1, y2, "Del"));
    }
    Coordinate_Compression();
    Node *root = new Node();
    build(root, 0, (int)compress.size());
    sort(SweepLine.begin(), SweepLine.end());
    ll ans = 0, last_cor = SweepLine[0].x;
    for(Event i : SweepLine) {
        ans += qry_uni(root) * (i.x - last_cor);
        if(i.type == "Add") {
            upd(root, compress[i.y1], compress[i.y2], 1);
        } else if(i.type == "Del") {
            upd(root, compress[i.y1], compress[i.y2], -1);
        }
        last_cor = i.x;
    }
    return ans;
}